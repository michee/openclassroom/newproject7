const jwt = require('jsonwebtoken'); 

module.exports = (req, res, next) => { 
    try {
        // Récupération du token dans le header dans un tableau split et on retourne le 2ème élément //
        const token = req.headers.authorization.split('')[1]; 
                // décodage du token, 
        const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET'); 
        const userId = decodedToken.userId; 
        req.decodedToken = decodedToken
        if (req.body.userId && req.body.userId !== userId) { 
            throw 'User ID non valable'; 
        } else {
            next();
        }
    } catch(error) {
        res.status(401).json({error: error.message});
    }
};
