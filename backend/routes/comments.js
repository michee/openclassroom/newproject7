const router = require('express').Router();
const commentCtrl = require('../controllers/commentController');
const auth = require('../middleware/auth');

// auth pour protéger les routes
router.post('/', auth,  commentCtrl.create); 
router.get('/:userid', auth, commentCtrl.getOne);
router.get('/:userid/allComments', commentCtrl.getAll);
router.put('/:id', auth, commentCtrl.modify);
router.delete('/:id', auth, commentCtrl.delete);


module.exports = router;