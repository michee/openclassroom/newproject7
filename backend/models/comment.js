'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class comment extends Model {
    
    static associate(models) {
      // define association here
      comment.belongsTo(models.User, {
        foreignKey: {
          allowNull: false
        }     
      })
      comment.hasMany(models.Post, {
        foreignKey: {
          allowNull: false
        },
        onDelete: "CASCADE"
      })
      
    }
  };
  comment.init({
    userId:{
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "User",
        key: "id"
      }
    },
    postId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: "Post",
        key: "id"
      }
    },
    content: DataTypes.STRING,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Comment',
  });
  // comment.associate = models => {
  //   comment.belongsTo(models.User, {
  //     foreignKey: {
  //       allowNull: false
  //     }     
  //   })
  //   comment.hasMany(models.Post, {
  //     foreignKey: {
  //       allowNull: false
  //     },
  //     onDelete: "CASCADE"
  //   })
  // }
  return comment;
};