const router = require('express').Router();
const postCtrl = require('../controllers/postController');
const auth = require('../middleware/auth')
const multer = require('../middleware/multer')

// auth pour protéger les routes
router.post('/',  multer, postCtrl.create); 
router.get('/:userid',  postCtrl.getOne);
router.get('/', postCtrl.getAll);
router.put('/:id', auth, multer, postCtrl.modify);
router.delete('/:id',  postCtrl.delete);


module.exports = router;
