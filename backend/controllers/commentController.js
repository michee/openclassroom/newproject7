const db = require('../models')


//Création d'un commentaire
exports.create = (req, res) => {
    db.comment.create({
            content: req.body.content,
            UserId: req.body.UserId,
            PostId: req.body.PostId
        })
        .then(newComment => res.send(newComment))
        .catch(() => res.status(500).send({
            message: "Impossible de créer un nouveau commentaire"
        }))
};

//affichage d'un commentaire
exports.getOne = (req, res) => {
    db.Comment.findOne({
            where: {
                id: req.params.id
            },
            include: [{
                model: db.User,
                attributes: {
                    exclude: ["password"]
                }
            }, {
                model: db.Post,
                include: [{
                    model: db.Post,
                    attributes: {
                        exclude: ["password"]
                    }
                }]
            }],

        }).then(comment => res.status(200).send(comment))
        .catch(() => {
            res.status(500).json({
                message: "Impossible d'afficher ce commentaire"
            })
        })
};
//affichage de tous les commentaires
exports.getAll = ((req, res) => {
    
    db.Comment.findAll({
        include: [{
            model: db.User,
            attributes: {
                exclude: ["password"]
            } 
        }],
        }, {
            model: db.Post,
            include: [{
                model: db.Post,
                attributes: {
                    exclude: ["password"]
                }
            }]
       
        })
        .then(allComments => {
            
            res.status(200).json({
                allComments
            })
        })
        .catch(() => res.status(500).json({
            message: "Impossible d'afficher les commentaires"
        }))
});

//Modification d'un commentaire
exports.modify = (req, res) => {
    db.User.findOne({
            where: {
                id: req.user.userId
            },
            attributes: {
                exclude: ["password"]
            }
        })
        .then(() => {
            const id = req.params.id
            //Recherche d'un commentaire en fonction de son id
            db.Comment.findOne({
                    where: {
                        id: id
                    }
                })
                .then(commentToUpdate => {
                    //vérification de l'utilisateur et de ses droits
                    if (req.user.userId !== commentToUpdate.UserId) {
                        return res.status(403).send({
                            message: "Modification non autorisée!"
                        })
                    }                   
                    //mise à jour du post
                    commentToUpdate.update({                           
                            content: req.body.content,                           
                        }, {
                            where: {
                                id: id
                            }
                        })
                        .catch(() => {
                            res.status(500).json({
                                message: "Impossible de modifier le commentaire"
                            })
                        })
                    res.status(200).send({
                        message: "ommentaire modifié avec succès!"
                    })
                })


        })


}

//suppression d'un commentaire
exports.delete = (req, res) => {
    const id = req.params.id;
    db.Comment.findOne({
            where: {
                id: id
            }
        })
        .then(comment => {
            if (req.user.userId !== comment.UserId && !req.user.isAdmin) {
                return res.status(403).send({
                    message: "Suppresion non autorisée!"
                })
            }       
            
            db.Post.destroy({
                    where: {
                        id: id
                    }
                })
                .then(() => {
                    res.status(200).json({
                        message: "Commentaire supprimé"
                    })
                })
                .catch(() => {
                    res.status(500).send({
                        message: "Impossible de supprimer le commentaire"
                    });
                });

        })

}