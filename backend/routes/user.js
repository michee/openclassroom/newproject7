const router = require('express').Router();
const userCtrl = require('../controllers/userController');
const auth = require('../middleware/auth');

// routes de création et identification de compte

router.post("/register",  userCtrl.register);
router.post("/login",  userCtrl.login);


// auth pour protéger les routes
router.get('/', userCtrl.getAll);
router.get('/:id', userCtrl.getOne);
router.put('/:id',auth,  userCtrl.modify);
router.delete('/:id', userCtrl.delete);


module.exports = router;