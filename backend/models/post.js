'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Post extends Model {
   
    static associate(models) {
      // define association here
      models.User.hasMany(models.Post,{
        onDelete: "CASCADE"
         })
       models.Post.belongsTo(models.User, {
          foreignKey: {
            allowNull: false
          },
         
        })    
    }
  };
  Post.init({
    // userId: {
    //   type: DataTypes.INTEGER,
    //   allowNull: false,
    //   references: {
    //     model: "Users",
    //     key: "id"
    //   }
    // },
    title: DataTypes.STRING,
    content: DataTypes.STRING,
    image: DataTypes.STRING,
    likes: DataTypes.INTEGER,
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Post',
  });
  // Post.associate = models => {
  //   Post.belongsTo(models.User, {
  //     foreignKey: {
  //       allowNull: false
  //     }     
  //   })
  //   Post.hasMany(models.Comment, {
  //     foreignKey: {
  //       allowNull: false
  //     },
  //     onDelete: "CASCADE"
  //   })
  // }
  return Post;
};