import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Signup from '../views/Signup.vue'
import Profil from '../views/Profil.vue'
import Post from '../views/Post.vue'
import PostList from '../components/PostList.vue'



Vue.use(VueRouter)
Vue.use(require('vue-moment'));

const routes = [  
  {
    path: '/',
    name: 'Login',
    component: Login,
  },  
    {
      path: '/signup',
      name: 'Signup',
      component: Signup,
    }, 
    {
      path: '/profil',
      name: 'Profil',
      component: Profil,
    },     
    {
      path: '/post',
      name: 'Post',
      component: Post,
    }, 
    {
      path: '/posts',
      name: 'PostList',
      component: PostList,
    },
  ]
  
  const router = new VueRouter({
    routes
  })
  
  export default router
  