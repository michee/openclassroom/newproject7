'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    
    static associate(models) {
      // define association here
      User.hasMany(models.Post, {
         onDelete: "CASCADE"
      })
          }
  };
  User.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      unique: true
    },
    password: DataTypes.STRING,
    isAdmin: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'User',
  });
  // User.associate = models => {
  //   User.hasMany(models.Post, {
  //     foreignKey: {
  //       allowNull: false
  //     },
  //     onDelete: "CASCADE"
  //   })
  //   User.hasMany(models.Comment, {
  //     foreignKey: {
  //       allowNull: false
  //     },
  //     onDelete: "CASCADE"
  //   })
  // }
  return User;
};