const jwt = require('jsonwebtoken')

module.exports = {
    
    parseAuthorization: function(authorization){
        //si authorization est different du null on le remplace par bearer
        return (authorization != null) ? authorization.replace('Bearer ', '') : null;
    },
    getUserId: function(authorization){
        //par sécurité on defini le userId sur -1 
        let userId = -1;
        let token = module.exports.parseAuthorization(authorization);
        //on verifie si le token et different de null 
        if (token != null) {
            try{
                //on verrifie le token avec la clé secrette 
                const jwtToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
                //si la verification est bonne on attribut la valeur a la constante userId 
                if (jwtToken != null)
                    userId = jwtToken.userId;
            } catch (err){}
        }
        //on retourne userId
        return userId
    }
}