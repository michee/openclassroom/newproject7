const db = require('../models')
const fs = require('fs');


//Création d'un post
exports.create = (req, res) => {
    //console.log(req.file);
//     
db.User.findOne({
    where: {id: req.body.userId}
})
.then(userFound => {
    db.Posts.create({
        title: "",
        content: "",
        images:""
    })
})
.then(newPost => {
    if(nexPost){ 
        return res.status(201).json(newPost)
    }else{
        return res.status(400).json({error :error.message})
    }
})
}
//affichage d'un post
exports.getOne = (req, res) => {
    db.Post.findOne({
            where: {
                id: req.params.id
            },
            include: [{
                model: db.User,
                attributes: {
                    exclude: ["password"]
                }
            }, {
                model: db.Comment,
                include: [{
                    model: db.User,
                    attributes: {
                        exclude: ["password"]
                    }
                }]
            }],

        }).then(post => res.status(200).send(post))
        .catch(() => {
            res.status(500).json({
                message: "Impossible d'afficher ce post"
            })
        })
}

//affichage de tous les posts
exports.getAll = (req, res) => {
    console.log(req);
    db.Post.findAll({
            include: [{
                model: db.User,
                attributes: {
                    exclude: ["password"]
                }
            }, {
                model: db.Comment
            }]
        })
        .then(allPosts => {
            // //console.log(allPosts);
            res.status(200).json({
                allPosts
            })
        })
        .catch(() => res.status(500).json({
            message: "Impossible d'afficher les posts"
        }))
}

//Modification d'un post
exports.modify = (req, res) => {
    db.User.findOne({
            where: {
                id: req.user.userId
            },
            attributes: {
                exclude: ["password"]
            }
        })
        .then(() => {
            const id = req.params.id
            //Recherche d'un post en fonction de son id
            db.Post.findOne({
                    where: {
                        id: id
                    }
                })
                .then(postToUpdate => {
                    //vérification de l'utilisateur et de ses droits
                    if (req.user.userId !== postToUpdate.UserId) {
                        return res.status(403).send({
                            message: "Modification non autorisée!"
                        })
                    }
                    //vérification de l'image
                    if (req.file) {
                        const filename = postToUpdate.image.split('/images/')[1]
                        fs.unlink(`images/${filename}`, () => {})
                        postToUpdate.image = `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
                    }
                    //mise à jour du post
                    postToUpdate.update({
                            title: req.body.title,
                            content: req.body.content,
                            image: postToUpdate.image
                        }, {
                            where: {
                                id: id
                            }
                        })
                        .catch(() => {
                            res.status(500).json({
                                message: "Impossible de modifier le post"
                            })
                        })
                    res.status(200).send({
                        message: "Post modifié avec succès!"
                    })
                })


        })


}

//suppression d'un post
exports.delete = (req, res) => {
    const id = req.params.id;
    db.Post.findOne({
            where: {
                id: id
            }
        })
        .then(post => {
            if (req.user.userId !== post.UserId && !req.user.isAdmin) {
                return res.status(403).send({
                    message: "Suppresion non autorisée!"
                })
            }
            const filename = post.image.split('/images/')[1]
            fs.unlink(`images/${filename}`, () => {})
            db.Post.destroy({
                    where: {
                        id: id
                    }
                })
                .then(() => {
                    res.status(200).json({
                        message: "Post supprimé"
                    })
                })
                .catch(() => {
                    res.status(500).send({
                        message: "Impossible de supprimer le post"
                    });
                });

        })

}


